FROM node:18 as baseImage

RUN apt update && apt upgrade -y

RUN mkdir -p ./code
WORKDIR /code
COPY ./ ./code


FROM baseImage as dev

EXPOSE 3000
CMD cd my-test-app && \
    npm i && \
    npm start
